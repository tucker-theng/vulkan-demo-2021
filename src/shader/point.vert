#version 450

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
    vec2 screenSize;
    float pointSize;
    float time;
    float noiseAmplitude;
    float noiseFrequency;
    float noiseApplyFrequency;
} ubo;
layout(binding = 2) uniform sampler2D noiseTexture;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 fragColor;

void main() {
    vec4 transformedPos = ubo.model * vec4(inPosition, 1.0);
    vec3 noiseSample = texture(noiseTexture, vec2(sin(inPosition.x + ubo.time * ubo.noiseFrequency), cos(inPosition.y + ubo.time * ubo.noiseFrequency))).rgb - vec3(0.5, 0.5, 0.5);
    vec4 warpedPos = transformedPos + ubo.noiseAmplitude * smoothstep(0, 1, abs(sin(ubo.time * ubo.noiseApplyFrequency)) - 0.5) * vec4(noiseSample, 0);
    gl_Position = ubo.proj * ubo.view * warpedPos;

    fragColor = inColor;

    gl_PointSize = ubo.pointSize / length(gl_Position);
}

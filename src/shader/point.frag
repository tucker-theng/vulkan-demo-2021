#version 450

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(fragColor, 1.0) * texture(texSampler, gl_PointCoord.st);
    if (outColor.a < 0.2)
        discard;
}

﻿#include <iostream>
#include "application/application.hpp"

int main() {
    auto app = std::make_shared<Application>();

    try {
        app->run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

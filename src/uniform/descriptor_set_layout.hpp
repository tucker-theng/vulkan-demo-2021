#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;

class DescriptorSetLayout
{
public:
    VkDescriptorSetLayout descriptorSetLayout;

    DescriptorSetLayout(std::shared_ptr<Application> app);
    ~DescriptorSetLayout();

private:
    std::shared_ptr<Application> app;
};

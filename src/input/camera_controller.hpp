#pragma once

#include <memory>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include "camera.hpp"

class Application;

class CameraController
{
public:
    Camera camera;

    CameraController(std::shared_ptr<Application> app);
    void update(float delta);
    glm::mat4 getViewMatrix();

private:
    std::shared_ptr<Application> app;
};

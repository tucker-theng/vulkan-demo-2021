#include "camera_controller.hpp"

#include "application/application.hpp"
#include <glfw/glfw3.h>

static void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    auto wrapper = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));

    static bool firstMouse = true;
    static float lastX = 1280 / 2.0f;
    static float lastY = 720 / 2.0f;
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    wrapper->app->cameraController->camera.ProcessMouseMovement(xoffset, yoffset);
}

static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    auto wrapper = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));

    wrapper->app->cameraController->camera.ProcessMouseScroll(yoffset);
}

CameraController::CameraController(std::shared_ptr<Application> app) : app(app)
{
    glfwSetCursorPosCallback(app->glfwWindow(), mouse_callback);
    glfwSetScrollCallback(app->glfwWindow(), scroll_callback);
}

void CameraController::update(float delta)
{
    if (glfwGetKey(app->glfwWindow(), GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, delta);
    if (glfwGetKey(app->glfwWindow(), GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, delta);
    if (glfwGetKey(app->glfwWindow(), GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, delta);
    if (glfwGetKey(app->glfwWindow(), GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, delta);

    if (glfwGetKey(app->glfwWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetInputMode(app->glfwWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    if (glfwGetKey(app->glfwWindow(), GLFW_KEY_ENTER) == GLFW_PRESS)
        glfwSetInputMode(app->glfwWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

glm::mat4 CameraController::getViewMatrix()
{
    return camera.GetViewMatrix();
}

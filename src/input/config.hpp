#pragma once

#include <string>
#include <memory>

class Application;

class Config
{
public:
    std::string path;

    double pointSize;
    std::string pointTexture;

    double modelScale;
    std::string modelPath;

    double noiseAmplitude;
    double noiseFrequency;
    double noiseApplyFrequency;

    Config(const std::string& path, std::shared_ptr<Application> app);
    void checkUpdates(float delta);

private:
    std::shared_ptr<Application> app;

    void updateVals(std::string path);
    void loadModel(std::string path);
    void loadPointTexture(std::string path);
};

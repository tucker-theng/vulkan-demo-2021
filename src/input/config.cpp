#include "config.hpp"

#include <INIReader.h>
#include <glfw/glfw3.h>
#include <algorithm>
#include "application/application.hpp"
#include "mesh/mesh_util.hpp"

const std::array<std::string, 4> texturePaths = {
	"assets/circle.png",
	"assets/rect.png",
	"assets/triangle.png",
	"assets/asterik.png"
};
const std::array<std::string, 6> modelPaths = {
	"assets/oirase_chosifalls.asc",
	"assets/hintze_hall.ply",
	"assets/tikal_guatemala.asc",
	"assets/mv_spartan.ply",
	"assets/king_mountain_cairn.ply",
	"assets/nebula3.ply"
};

static size_t wrap(size_t val, size_t max)
{
	if (val < 0)
		return 0;
	if (val >= max)
		return 0;
	return val;
}

Config::Config(const std::string& path, std::shared_ptr<Application> app) : app(app), path(path)
{
	updateVals(path);
}

void Config::checkUpdates(float delta)
{
	if (glfwGetKey(app->glfwWindow(), GLFW_KEY_R) == GLFW_PRESS)
		updateVals(path);

	static bool modelPressed = false;
	static int modelIndex = 0;
	if (glfwGetKey(app->glfwWindow(), GLFW_KEY_M) == GLFW_PRESS && !modelPressed)
	{
		modelPressed = true;
		modelIndex = wrap(modelIndex + 1, modelPaths.size());
		loadModel(modelPaths[modelIndex]);
	}
	else if (glfwGetKey(app->glfwWindow(), GLFW_KEY_M) == GLFW_RELEASE)
	{
		modelPressed = false;
	}

	static bool texturePressed = false;
	static int textureIndex = 0;
	if (glfwGetKey(app->glfwWindow(), GLFW_KEY_T) == GLFW_PRESS && !texturePressed)
	{
		texturePressed = true;
		textureIndex = wrap(textureIndex + 1, texturePaths.size());
		loadPointTexture(texturePaths[textureIndex]);
	}
	else if (glfwGetKey(app->glfwWindow(), GLFW_KEY_T) == GLFW_RELEASE)
	{
		texturePressed = false;
	}
}

void Config::updateVals(std::string path)
{
    INIReader reader(path);

	pointSize = reader.GetReal("point", "size", 20);
	pointTexture = reader.GetString("point", "texture", "assets/circle.png");

	modelScale = reader.GetReal("model", "scale", 1.0);
	modelPath = reader.GetString("model", "path", "assets/oirase_chosifalls.asc");

	noiseAmplitude = reader.GetReal("noise", "amplitude", 1.0);
	noiseFrequency = reader.GetReal("noise", "frequency", 0.1);
	noiseApplyFrequency = reader.GetReal("noise", "applyFrequency", 0.2);

	if (app->texture != nullptr && pointTexture != app->texture->path)
	{
		loadPointTexture(pointTexture);
	}

	if (app->vertexBuffer != nullptr && app->modelPath != modelPath)
	{
		loadModel(modelPath);
	}
}

void Config::loadModel(std::string path)
{
	app->vertexBuffer = std::make_shared<VertexBuffer>(app, getMeshVertices(path));
	app->modelPath = modelPath;
	app->swapChain->forceRecreate = true;
}

void Config::loadPointTexture(std::string path)
{
	app->texture = std::make_shared<Texture>(app, path);
	app->swapChain->forceRecreate = true;
}

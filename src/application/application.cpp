#include "application.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <gtx/euler_angles.hpp>
#include <chrono>
#include "mesh/vertex.hpp"
#include "mesh/mesh_util.hpp"

Application::Application()
{

}

Application::~Application()
{
}

void Application::init()
{
    config = std::make_shared<Config>("config.ini", shared_from_this());
    window = std::make_shared<Window>(shared_from_this(), 1280, 720, "Vulkan Demo");
    instance = std::make_shared<Instance>();
    surface = std::make_shared<Surface>(shared_from_this());
    device = std::make_shared<Device>(shared_from_this());
    commandPool = std::make_shared<CommandPool>(shared_from_this());
    texture = std::make_shared<Texture>(shared_from_this(), "assets/circle.png");
    noiseTexture = std::make_shared<Texture>(shared_from_this(), "assets/noise.jpg");
    descriptorSetLayout = std::make_shared<DescriptorSetLayout>(shared_from_this());
    vertexBuffer = std::make_shared<VertexBuffer>(shared_from_this(), getMeshVertices("assets/oirase_chosifalls.asc"));
    swapChain = std::make_shared<SwapChain>(shared_from_this());
    swapChain->create();
    cameraController = std::make_shared<CameraController>(shared_from_this());
}

UniformBufferObject Application::createFrameUBO()
{
    static auto startTime = std::chrono::high_resolution_clock::now();

    auto currentTime = std::chrono::high_resolution_clock::now();
    float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

    UniformBufferObject ubo{};
    ubo.model = glm::scale(glm::mat4(1.0f), glm::vec3(config->modelScale, config->modelScale, config->modelScale));
    ubo.view = cameraController->getViewMatrix() * glm::eulerAngleYXZ(glm::radians(0.0f), glm::radians(-90.0f), glm::radians(0.0f));
    ubo.proj = glm::perspective(glm::radians(45.0f), swapChain->swapChainExtent.width / (float)swapChain->swapChainExtent.height, 0.1f, 100.0f);

    // Correct for difference between OpenGL and Vulkan coordinates
    ubo.proj[1][1] *= -1;

    ubo.screenSize.x = swapChain->swapChainExtent.width;
    ubo.screenSize.y = swapChain->swapChainExtent.height;

    ubo.pointSize = config->pointSize;

    ubo.time = time;

    ubo.noiseAmplitude = config->noiseAmplitude;
    ubo.noiseFrequency = config->noiseFrequency;
    ubo.noiseApplyFrequency = config->noiseApplyFrequency;

    return ubo;
}

void Application::run()
{
    init();

    auto lastTime = std::chrono::high_resolution_clock::now();
    while (!window->shouldClose()) {
        window->pollEvents();

        auto currentTime = std::chrono::high_resolution_clock::now();
        float delta = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - lastTime).count();
        lastTime = currentTime;
        config->checkUpdates(delta);
        cameraController->update(delta);

        swapChain->drawFrame(createFrameUBO());
    }

    vkDeviceWaitIdle(vkLogicalDevice());
}

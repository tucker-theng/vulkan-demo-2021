#pragma once

#include <memory>
#include "window.hpp"
#include "instance.hpp"
#include "surface.hpp"
#include "device.hpp"
#include "swap_chain.hpp"
#include "render_pass.hpp"
#include "command_pool.hpp"
#include "texture/texture.hpp"
#include "mesh/vertex_buffer.hpp"
#include "uniform/descriptor_set_layout.hpp"
#include "input/camera_controller.hpp"
#include "input/config.hpp"

class Application : public std::enable_shared_from_this<Application>
{
public:
    Application();
    ~Application();

    void run();

    GLFWwindow* glfwWindow() const { return window->window; }
    VkInstance vkInstance() const { return instance->instance; }
    VkSurfaceKHR vkSurface() const { return surface->surface; }
    VkPhysicalDevice vkPhysicalDevice() const { return device->physicalDevice; }
    VkDevice vkLogicalDevice() const { return device->logicalDevice; }
    VkQueue vkGraphicsQueue() const { return device->graphicsQueue; }
    VkQueue vkPresentQueue() const { return device->presentQueue; }
    VkCommandPool vkCommandPool() const { return commandPool->commandPool; }
    VkRenderPass vkRenderPass() const { return swapChain->renderPass->renderPass; }
    VkBuffer vkVertexBuffer() const { return vertexBuffer->vertexBuffer; }
    VkDescriptorSetLayout* vkDescriptorSetLayout() const { return &descriptorSetLayout->descriptorSetLayout; }

    std::shared_ptr<Config> config;
    std::shared_ptr<Window> window;
    std::shared_ptr<Instance> instance;
    std::shared_ptr<Surface> surface;
    std::shared_ptr<Device> device;
    std::shared_ptr<CommandPool> commandPool;
    std::shared_ptr<Texture> texture;
    std::shared_ptr<Texture> noiseTexture;
    std::shared_ptr<VertexBuffer> vertexBuffer;
    std::shared_ptr<SwapChain> swapChain;
    std::shared_ptr<DescriptorSetLayout> descriptorSetLayout;
    std::shared_ptr<CameraController> cameraController;
    std::string modelPath;

    void init();

private:
    UniformBufferObject createFrameUBO();

};

#include "color_buffer.hpp"

#include "application.hpp"
#include "util/image_util.hpp"

ColorBuffer::ColorBuffer(std::shared_ptr<Application> app) : app(app)
{
    VkFormat colorFormat = app->swapChain->swapChainImageFormat;

    createImage(app,
        app->swapChain->swapChainExtent.width, app->swapChain->swapChainExtent.height, 1, app->device->msaaSamples,
        colorFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, colorImage, colorImageMemory);
    colorImageView = createImageView(app->vkLogicalDevice(), colorImage, colorFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

ColorBuffer::~ColorBuffer()
{
    vkDestroyImageView(app->vkLogicalDevice(), colorImageView, nullptr);
    vkDestroyImage(app->vkLogicalDevice(), colorImage, nullptr);
    vkFreeMemory(app->vkLogicalDevice(), colorImageMemory, nullptr);
}

void ColorBuffer::createColorBuffer()
{
}

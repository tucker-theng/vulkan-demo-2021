#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;

class GraphicsPipeline
{
public:
    VkPipelineLayout pipelineLayout;
    VkPipeline graphicsPipeline;
    GraphicsPipeline(std::shared_ptr<Application> app, VkExtent2D extents, VkRenderPass renderPass);
    ~GraphicsPipeline();

private:
    std::shared_ptr<Application> app;
};

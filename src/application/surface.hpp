#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;
class Window;
class Instance;

class Surface
{
public:
    VkSurfaceKHR surface;

    Surface(const std::shared_ptr<Application> app);
    ~Surface();

private:
    std::shared_ptr<Application> app;
};

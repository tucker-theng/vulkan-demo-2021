#pragma once

#include <vulkan/vulkan.hpp>

class Application;

class Device
{
public:
    VkPhysicalDevice physicalDevice;
    VkDevice logicalDevice;
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_1_BIT;

    Device(std::shared_ptr<Application> app);
    ~Device();

private:
    std::shared_ptr<Application> app;

    void pickPhysicalDevice();
    void createLogicalDevice();
};

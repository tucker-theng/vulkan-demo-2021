#include "depth_buffer.hpp"

#include "application.hpp"
#include "util/image_util.hpp"

DepthBuffer::DepthBuffer(std::shared_ptr<Application> app) : app(app)
{
    createDepthBuffer();
}

DepthBuffer::~DepthBuffer()
{
    vkDestroyImageView(app->vkLogicalDevice(), depthImageView, nullptr);
    vkDestroyImage(app->vkLogicalDevice(), depthImage, nullptr);
    vkFreeMemory(app->vkLogicalDevice(), depthImageMemory, nullptr);
}

void DepthBuffer::createDepthBuffer()
{
    VkFormat depthFormat = findDepthFormat(app);

    createImage(app, app->swapChain->swapChainExtent.width, app->swapChain->swapChainExtent.height, 1, app->device->msaaSamples, depthFormat,
        VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        depthImage, depthImageMemory);
    depthImageView = createImageView(app->vkLogicalDevice(), depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);
    transitionImageLayout(app, depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);
}

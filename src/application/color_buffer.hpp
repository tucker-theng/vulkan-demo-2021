#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;

class ColorBuffer
{
public:
    VkImage colorImage;
    VkDeviceMemory colorImageMemory;
    VkImageView colorImageView;

    ColorBuffer(std::shared_ptr<Application> app);
    ~ColorBuffer();

private:
    std::shared_ptr<Application> app;

    void createColorBuffer();
};

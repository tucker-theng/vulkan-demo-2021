#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;

class CommandPool
{
public:
    VkCommandPool commandPool;
    CommandPool(std::shared_ptr<Application> app);
    ~CommandPool();

private:
    std::shared_ptr<Application> app;
};

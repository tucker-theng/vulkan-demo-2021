#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;

class DepthBuffer
{
public:
    VkImage depthImage;
    VkDeviceMemory depthImageMemory;
    VkImageView depthImageView;

    DepthBuffer(std::shared_ptr<Application> app);
    ~DepthBuffer();

private:
    std::shared_ptr<Application> app;

    void createDepthBuffer();
};


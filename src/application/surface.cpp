#include "surface.hpp"
#include "application.hpp"
#include "instance.hpp"
#include "window.hpp"

Surface::Surface(const std::shared_ptr<Application> app) : app(app)
{
    if (glfwCreateWindowSurface(app->vkInstance(), app->glfwWindow(), nullptr, &surface) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create window surface.");
    }
}

Surface::~Surface()
{
    // Destroy surface
    vkDestroySurfaceKHR(app->vkInstance(), surface, nullptr);
}

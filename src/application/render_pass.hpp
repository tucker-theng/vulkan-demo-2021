#pragma once

#include <memory>
#include <vulkan/vulkan.hpp>

class Application;

class RenderPass
{
public:
    VkRenderPass renderPass;
    RenderPass(std::shared_ptr<Application> app, VkFormat imageFormat);
    ~RenderPass();

private:
    std::shared_ptr<Application> app;
};

#pragma once

#include <vulkan/vulkan.hpp>

class Instance
{
public:
    VkInstance instance;

    Instance();
    ~Instance();

private:
    VkDebugUtilsMessengerEXT debugMessenger;

    void createDebugMessenger();
    void createInstance();
};

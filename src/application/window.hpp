#pragma once

#include <glfw/glfw3.h>
#include <string>
#include <memory>

class Application;

class Window
{
public:
    GLFWwindow* window;
    std::shared_ptr<Application> app;

    Window(std::shared_ptr<Application> app, int width, int height, std::string title);
    ~Window();

    bool shouldClose() const;
    void pollEvents() const;

    bool sizeDirty() const;
    void clearSizeDirty();
    void setSizeDirty();

private:
    bool resized;
};

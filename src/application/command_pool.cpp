#include "command_pool.hpp"

#include "requirements.hpp"
#include "application.hpp"

CommandPool::CommandPool(std::shared_ptr<Application> app) : app(app)
{
    QueueFamilyIndices queueFamilyIndices = findQueueFamilies(app->vkPhysicalDevice(), app->vkSurface());

    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
    poolInfo.flags = 0;

    if (vkCreateCommandPool(app->vkLogicalDevice(), &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create command pool.");
    }
}

CommandPool::~CommandPool()
{
    vkDestroyCommandPool(app->vkLogicalDevice(), commandPool, nullptr);
}

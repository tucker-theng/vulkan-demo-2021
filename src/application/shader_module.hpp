#pragma once

#include <vulkan/vulkan.hpp>
#include <string>
#include <memory>

class Application;

class ShaderModule
{
public:
    VkShaderModule shaderModule;

    ShaderModule(std::shared_ptr<Application> app, const std::string& filename);
    ~ShaderModule();

private:
    std::shared_ptr<Application> app;
};

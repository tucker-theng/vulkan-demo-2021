#pragma once

#include <memory>
#include <vector>
#include <vulkan/vulkan.hpp>
#include "uniform/uniform_buffer_object.hpp"

class Application;
class RenderPass;
class GraphicsPipeline;
class DepthBuffer;
class ColorBuffer;

class SwapChain
{
public:
    VkSwapchainKHR swapChain;
    std::vector<VkImage> swapChainImages;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    std::vector<VkImageView> swapChainImageViews;
    std::vector<VkFramebuffer> swapChainFramebuffers;
    std::shared_ptr<DepthBuffer> depthBuffer;
    std::shared_ptr<ColorBuffer> colorBuffer;
    std::vector<VkCommandBuffer> commandBuffers;
    std::vector<VkBuffer> uniformBuffers;
    VkDescriptorPool descriptorPool;
    std::vector<VkDescriptorSet> descriptorSets;
    std::vector<VkDeviceMemory> uniformBuffersMemory;
    std::vector<VkSemaphore> imageAvailableSemaphores;
    std::vector<VkSemaphore> renderFinishedSemaphores;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;
    size_t currentFrame = 0;

    std::shared_ptr<RenderPass> renderPass;
    std::shared_ptr<GraphicsPipeline> graphicsPipeline;

    bool forceRecreate = false;

    SwapChain(const std::shared_ptr<Application> app);
    ~SwapChain();

    void create();
    void drawFrame(UniformBufferObject ubo);
    void recreate();

private:
    std::shared_ptr<Application> app;

    void createSwapChain();
    void createImageViews();
    void createRenderPass();
    void createGraphicsPipeline();
    void createUniformBuffers();
    void createDescriptorPool();
    void createDescriptorSets();
    void createFramebuffers();
    void createDepthBuffer();
    void createColorBuffer();
    void createCommandBuffers();
    void createSyncObjects();

    void updateUniformBuffer(uint32_t index, UniformBufferObject ubo);

    void cleanup();
};

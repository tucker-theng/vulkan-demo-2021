#include "window.hpp"

#include <stdexcept>
#include "application.hpp"

// Throw any errors from GLFW as exceptions
static void glfwErrorCallback(int error, const char* description) {
    throw std::runtime_error(description);
}

// Callback from framebuffer resizes
static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
    auto wrapper = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    wrapper->setSizeDirty();
}

Window::Window(std::shared_ptr<Application> app, int width, int height, std::string title)
    : app(app), resized(false)
{
    // Initialize GLFW
    glfwSetErrorCallback(glfwErrorCallback);
    if (!glfwInit()) {
        throw std::runtime_error("Failed to initialize GLFW.");
    }

    // Vulkan -> NO_API
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

    // Connect to frame resize callback
    glfwSetWindowUserPointer(window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);

    if (!window) {
        glfwTerminate();
        throw std::runtime_error("Failed to create GLFW window.");
    }
}

Window::~Window()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

bool Window::shouldClose() const
{
    return glfwWindowShouldClose(window);
}

void Window::pollEvents() const
{
    glfwPollEvents();
}

bool Window::sizeDirty() const
{
    return resized;
}

void Window::clearSizeDirty()
{
    resized = false;
}

void Window::setSizeDirty()
{
    resized = true;
}

#include "shader_module.hpp"

#include <iostream>
#include <fstream>
#include "application.hpp"

static std::vector<char> readFile(const std::string& filename) {
    std::ifstream file(filename + ".spv", std::ios::ate | std::ios::binary);

    if (!file.is_open()) {
        throw std::runtime_error("Failed to open shader file.");
    }

    size_t fileSize = (size_t)file.tellg();
    std::vector<char> buffer(fileSize);
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    return buffer;
}

ShaderModule::ShaderModule(std::shared_ptr<Application> app, const std::string& filename) : app(app)
{
    // Read in the shader file
    auto code = readFile(filename);

    // Point create info to SPV code
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = code.size();
    createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

    if (vkCreateShaderModule(app->vkLogicalDevice(), &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create shader info");
    }
}

ShaderModule::~ShaderModule()
{
    vkDestroyShaderModule(app->vkLogicalDevice(), shaderModule, nullptr);
}

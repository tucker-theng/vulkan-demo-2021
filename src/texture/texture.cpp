#include "texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include "application/application.hpp"
#include "util/image_util.hpp"
#include "util/command_util.hpp"
#include "util/buffer_util.hpp"

Texture::Texture(std::shared_ptr<Application> app, const std::string& path) : app(app), path(path)
{
    createTextureImage(path);
    createTextureImageView();
    // TODO move sampler to its own class (they're independent of textures in Vulkan)
    createTextureSampler();
}

Texture::~Texture()
{
    vkDestroySampler(app->vkLogicalDevice(), textureSampler, nullptr);
    vkDestroyImageView(app->vkLogicalDevice(), textureImageView, nullptr);
    vkDestroyImage(app->vkLogicalDevice(), textureImage, nullptr);
    vkFreeMemory(app->vkLogicalDevice(), textureImageMemory, nullptr);
}

void Texture::createTextureImage(const std::string& path)
{
    // Load image file with STB
    int texWidth, texHeight, texChannels;
    stbi_uc* pixels = stbi_load(path.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    VkDeviceSize imageSize = texWidth * texHeight * 4;

    // Calculate mipmap levels
    mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(texWidth, texHeight)))) + 1;

    if (!pixels) {
        throw std::runtime_error("Failed to load texture image.");
    }

    // Copy image data into staging buffer
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(app->vkLogicalDevice(), app->vkPhysicalDevice(),
        imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer, stagingBufferMemory);
    void* data;
    vkMapMemory(app->vkLogicalDevice(), stagingBufferMemory, 0, imageSize, 0, &data);
    memcpy(data, pixels, static_cast<size_t>(imageSize));
    vkUnmapMemory(app->vkLogicalDevice(), stagingBufferMemory);

    stbi_image_free(pixels);

    createImage(app,
        texWidth, texHeight, mipLevels, VK_SAMPLE_COUNT_1_BIT,
        VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        textureImage, textureImageMemory);

    // Transition image to layout for copy target
    transitionImageLayout(app, textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels);
    // Copy staging buffer to image
    copyBufferToImage(app, stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
    // Generate mipmaps and transition image to layout for shader reads
    generateMipmaps(app, textureImage, VK_FORMAT_R8G8B8A8_SRGB, texWidth, texHeight, mipLevels);

    // Cleanup staging buffer
    vkDestroyBuffer(app->vkLogicalDevice(), stagingBuffer, nullptr);
    vkFreeMemory(app->vkLogicalDevice(), stagingBufferMemory, nullptr);
}

void Texture::createTextureImageView()
{
    textureImageView = createImageView(app->vkLogicalDevice(), textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT, mipLevels);
}

void Texture::createTextureSampler()
{
    // Query device properties for sampler options
    VkPhysicalDeviceProperties properties{};
    vkGetPhysicalDeviceProperties(app->vkPhysicalDevice(), &properties);

    VkSamplerCreateInfo samplerInfo{};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = static_cast<float>(mipLevels);

    if (vkCreateSampler(app->vkLogicalDevice(), &samplerInfo, nullptr, &textureSampler) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create texture sampler.");
    }
}

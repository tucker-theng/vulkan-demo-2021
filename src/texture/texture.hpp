#pragma once

#include <memory>
#include <string>
#include <vulkan/vulkan.hpp>

class Application;

class Texture
{
public:
    int mipLevels;
    VkImage textureImage;
    VkDeviceMemory textureImageMemory;
    VkImageView textureImageView;
    VkSampler textureSampler;
    std::string path;

    Texture(std::shared_ptr<Application> app, const std::string& path);
    ~Texture();

private:
    std::shared_ptr<Application> app;

    void createTextureImage(const std::string& path);
    void createTextureImageView();
    void createTextureSampler();
};

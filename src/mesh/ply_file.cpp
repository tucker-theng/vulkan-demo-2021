#include "ply_file.hpp"

#include <happly.h>
#include <glm/gtc/color_space.hpp>
#include "vertex.hpp"

PLYFile::PLYFile(const std::string& path)
{
    loadPLYFile(path);
}

void PLYFile::loadPLYFile(const std::string& path)
{
    happly::PLYData plyIn(path);

    std::vector<std::array<double, 3>> vertexPositions = plyIn.getVertexPositions();
    std::vector<std::array<unsigned char, 3>> vertexColors = plyIn.getVertexColors();

    glm::dvec3 center;

    vertices.resize(vertexPositions.size());
    for (int i = 0; i < vertexPositions.size(); i++)
    {
        Vertex vert;
        vert.pos.x = vertexPositions[i][0];
        vert.pos.y = vertexPositions[i][1];
        vert.pos.z = vertexPositions[i][2];
        // TODO gamma correct
        vert.color.r = vertexColors[i][0] / 255.0f;
        vert.color.g = vertexColors[i][1] / 255.0f;
        vert.color.b = vertexColors[i][2] / 255.0f;
        vert.color = glm::convertSRGBToLinear(vert.color);
        vertices[i] = vert;
        center += vert.pos;
    }
    center /= vertices.size();

    for (int i = 0; i < vertices.size(); i++)
    {
        vertices[i].pos -= center;
    }
}

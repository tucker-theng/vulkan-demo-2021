#pragma once

#include <string>
#include "mesh/vertex.hpp"

class PLYFile
{
public:
    std::vector<Vertex> vertices;

    PLYFile(const std::string& path);

private:
    void loadPLYFile(const std::string& path);
};

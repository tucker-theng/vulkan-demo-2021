#pragma once

#include <string>
#include "mesh/vertex.hpp"

class ASCFile
{
public:
    std::vector<Vertex> vertices;

    ASCFile(const std::string& path);

private:
    void loadASCFile(const std::string& path);
};

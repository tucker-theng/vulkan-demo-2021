#include "mesh_util.hpp"

#include <string>
#include <locale>
#include "asc_file.hpp"
#include "ply_file.hpp"

std::string toLower(const std::string& str)
{
    std::locale loc;
    std::stringstream out;
    for (auto c : str)
        out << std::tolower(c, loc);
    return out.str();
}

std::vector<Vertex> getMeshVertices(const std::string& path)
{

    std::string extension = path.substr(path.find_last_of(".") + 1);

    if (toLower(extension) == "asc")
    {
        auto file = ASCFile(path);
        return std::move(file.vertices);
    }
    else if (toLower(extension) == "ply")
    {
        auto file = PLYFile(path);
        return std::move(file.vertices);
    }
    else
    {
        throw std::runtime_error("File format not supported");
    }
}

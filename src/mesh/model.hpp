#pragma once

#include <string>
#include "mesh/vertex.hpp"

class Model
{
public:
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;

    Model(const std::string& path);

private:
    void loadGLTFModel(const std::string& path);
};

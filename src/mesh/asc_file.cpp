#include "asc_file.hpp"

#include <iostream>
#include <fstream>
#include <glm/gtc/color_space.hpp>

enum class ParseState {X, Y, Z, R, G, B, D};

// Higher precision vertex, for models with weirdly large distance from 0
struct DVertex
{
    glm::dvec3 pos;
    glm::vec3 color;
};

static DVertex parseLine(const std::string& line)
{
    DVertex vert;

    std::stringstream ss(line);
    std::string elem;
    ParseState state = ParseState::X;
    // For each space seperated element on the line
    while (std::getline(ss, elem, ' '))
    {
        switch (state)
        {
        case ParseState::X:
            vert.pos.x = std::stod(elem);
            break;
        case ParseState::Y:
            vert.pos.y = std::stod(elem);
            break;
        case ParseState::Z:
            vert.pos.z = std::stod(elem);
            break;
        case ParseState::R:
            vert.color.r = std::stoi(elem) / 255.0f;
            break;
        case ParseState::G:
            vert.color.g = std::stoi(elem) / 255.0f;
            break;
        case ParseState::B:
            vert.color.b = std::stoi(elem) / 255.0f;
            break;
        }
        state = ParseState((int)state + 1);
    }
    vert.color = glm::convertSRGBToLinear(vert.color);

    return vert;
}

ASCFile::ASCFile(const std::string& path)
{
    loadASCFile(path);
}

void ASCFile::loadASCFile(const std::string& path)
{
    std::fstream file;
    file.open(path, std::ios::in);
    if (file.is_open())
    {
        std::string line;
        std::vector<DVertex> dvertices;
        glm::dvec3 center;
        // For each point in the file
        while (std::getline(file, line))
        {
            auto vert = parseLine(line);
            center += vert.pos;
            dvertices.push_back(vert);
        }
        size_t count = dvertices.size();
        center /= count;

        // Recenter the model
        for (int i = 0; i < dvertices.size(); i++)
        {
            dvertices[i].pos = dvertices[i].pos - center;
        }

        // Convert from double to float
        vertices.resize(dvertices.size());
        for (int i = 0; i < vertices.size(); i++)
        {
            vertices[i].pos = dvertices[i].pos;
            vertices[i].color = dvertices[i].color;
        }
    }
    file.close();
}

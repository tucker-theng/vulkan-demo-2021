#include "vertex_buffer.hpp"

#include "vertex.hpp"
#include "application/application.hpp"
#include "util/buffer_util.hpp"

VertexBuffer::VertexBuffer(std::shared_ptr<Application> app, const std::vector<Vertex>& vertices) : app(app)
{
    createVertexBuffer(vertices);
}

VertexBuffer::~VertexBuffer()
{
    vkDestroyBuffer(app->vkLogicalDevice(), vertexBuffer, nullptr);
    vkFreeMemory(app->vkLogicalDevice(), vertexMemory, nullptr);
}

void VertexBuffer::createVertexBuffer(const std::vector<Vertex>& vertices)
{
    vertexCount = vertices.size();
    VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

    // Create staging buffer, visible to CPU
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(app->vkLogicalDevice(), app->vkPhysicalDevice(),
        bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer, stagingBufferMemory);

    // Copy vertex data to staging buffer
    void* data;
    vkMapMemory(app->vkLogicalDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, vertices.data(), (size_t)bufferSize);
    vkUnmapMemory(app->vkLogicalDevice(), stagingBufferMemory);

    // Create the actual, optimized for GPU read
    createBuffer(app->vkLogicalDevice(), app->vkPhysicalDevice(),
        bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        vertexBuffer, vertexMemory);

    // Copy from staging to actual buffer
    copyBuffer(app, stagingBuffer, vertexBuffer, bufferSize);

    vkDestroyBuffer(app->vkLogicalDevice(), stagingBuffer, nullptr);
    vkFreeMemory(app->vkLogicalDevice(), stagingBufferMemory, nullptr);
}

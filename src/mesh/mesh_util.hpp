#pragma once

#include <vector>
#include "vertex.hpp"

std::string toLower(const std::string& str);

std::vector<Vertex> getMeshVertices(const std::string& path);

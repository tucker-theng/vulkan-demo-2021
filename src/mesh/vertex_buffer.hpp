#pragma once

#include <memory>
#include <vector>
#include <vulkan/vulkan.hpp>

class Application;
struct Vertex;

class VertexBuffer
{
public:
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexMemory;

    size_t vertexCount;

    VertexBuffer(std::shared_ptr<Application> app, const std::vector<Vertex>& vertices);
    ~VertexBuffer();

private:
    std::shared_ptr<Application> app;

    void createVertexBuffer(const std::vector<Vertex>& vertices);
};
